import { Injectable } from '@angular/core';
import { Contacts } from '@ionic-native/contacts';

/*
  Generated class for the TestProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/

@Injectable()
export class ContactsService {

  public allContacts: any;

  constructor(public contacts: Contacts) {
  }

  getAllContacts() {
    this.contacts.find(['displayName', 'name', 'phoneNumbers', 'emails'], {filter: "", multiple: true}).then(res => {
        this.allContacts = res;
    })
  }
}