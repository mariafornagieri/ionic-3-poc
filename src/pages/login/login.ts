import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, LoadingController, AlertController } from 'ionic-angular';
import { Storage } from '@ionic/storage'
import { SocialSharing } from '@ionic-native/social-sharing';

import { LoginService } from './login.service';
import { ContactsService } from '../../common/contacts.service';
import { User } from '../../models/user';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  user: User = {
    email: '',
    password: ''
  }

  constructor(public navCtrl: NavController, public navParams: NavParams, public platform: Platform, 
              public loginService: LoginService, public contactsService: ContactsService, public storage: Storage,
              public loadingCtrl: LoadingController, public alertCtrl: AlertController, public socialSharing: SocialSharing) {
  }

  ionViewDidLoad() {
    this.platform.ready().then(res => {
      this.contactsService.getAllContacts();
    })
  }

  goToRegisterPage() {
    this.navCtrl.push('RegisterPage');
  }

  goToForgotPasswordPage() {
    this.navCtrl.push('PasswordPage');
  }

  sendEmail() {
    this.socialSharing.shareViaEmail(
      '', '', ['meajuda@appdindin.com']
    )
  }

  atualizaPerfil() {
    // do something;
  }

  loginUser() {
    let loader = this.loadingCtrl.create();
    loader.present();
    this.validateFormData(this.user);
    this.loginService.login(this.user).subscribe(res => {
      this.storage.set('dindin.usuario_id', res.data.id);
      this.atualizaPerfil();
      loader.dismiss();
    } , err => {
      let errorAlert = this.alertCtrl.create();
      errorAlert.present();
    });
  }

  validateFormData(data: User) {
    if (!this.user.email || !this.user.password) {
      this.alertCtrl.create();
    }
    return;
  }

}
