import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http'
import { Observable } from 'rxjs/Observable';

/*
  Generated class for the TestProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class LoginService {

  constructor(public http: Http) {
  }

  createAuthorizationHeader(headers: Headers) {
    headers.append('Authorization', 'Basic ZGluZGluX3JlbmF0bzpkaW5kaW4=');
  }

  login(data): Observable<{}> {
    let headers = new Headers();
    return this.http.post('api/login', data, {headers: headers});
  }

}